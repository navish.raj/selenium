package testNG_Navisraj.Oct_24;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class journalHelper {

    public WebDriver driver;
    String baseUrl = "https://mc.manuscriptcentral.com/";

    public void chromeSetup()
    {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/windows/chromedriver_94.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    public void loginDetails(String url, String username, String password, String expectedTitle){
        driver.get(baseUrl + url);
        String actualTitle = driver.findElements(By.tagName("h2")).get(1).getText();
        Assert.assertEquals(actualTitle,expectedTitle);
        driver.findElement(By.name("USERID")).sendKeys(username);
        driver.findElement(By.name("PASSWORD")).sendKeys(password);
        driver.findElement(By.id("logInButton")).click();
    }
    
}
