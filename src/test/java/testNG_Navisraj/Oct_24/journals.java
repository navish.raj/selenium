package testNG_Navisraj.Oct_24;

import org.openqa.selenium.By;
import org.testng.annotations.*;

public class journals extends journalHelper {

    @BeforeMethod
    public void driverSetup(){
    chromeSetup();
    }

    @AfterMethod
    public void quit(){driver.quit();}

    @Test
    public void journalAJH(){
        loginDetails("ajh","ajhadmin@wiley.com","Smile@500", "American Journal of Hematology");
        driver.findElement(By.xpath("//*[@class='nav']//li[4]")).click();
        driver.findElement(By.linkText("Administrative Center")).click();
        String adminChecklist = driver.findElement(By.xpath("//*[@class='tablelightcolor']//table//tr[4]//td[2]")).getText();
        System.out.println("AJH total submissions number " +adminChecklist);
    }

    @Test
    public void journalNAE(){
        loginDetails("nurseauthoreditor","nae.office@wiley.com","Wiley#nae20", "Nurse Author & Editor");
        driver.findElement(By.xpath("//*[@class='nav']//li[4]")).click();
        driver.findElement(By.linkText("Admin Center")).click();
        String adminChecklist = driver.findElement(By.xpath("//*[@class='tablelightcolor']//table//tr[3]//td[2]")).getText();
        System.out.println("NAE total submissions number " +adminChecklist);
    }

    @Test
    public void journalECHO(){
        loginDetails("echo","echocardiography@wiley.com","Smile@500", "Echocardiography");
        driver.findElement(By.xpath("//*[@class='nav']//li[4]")).click();
        //driver.findElement(By.linkText("Admin Center")).click();
        String adminChecklist = driver.findElement(By.xpath("//*[@class='tablelightcolor']//table//tr[3]//td[2]")).getText();
        String submissionsInDrafts = driver.findElement(By.xpath("//*[@class='tablelightcolor']//table//tr[4]//td[2]")).getText();
        System.out.println("ECHO total submissions number " +adminChecklist);
        System.out.println("ECHO total submissions in drafts " +submissionsInDrafts);
    }
}
