package javaProject_beasant.oct15_21;

public class child_1_Inheritance extends parent_Inheritance {
    int a = 10;
    int b = 15;
    String address = "No 16, Karpagavinayagar Nagar, Tirunelveli - 7";
    public void sum(){
        int sum = a+b;
        System.out.println("the sum of two number is " +sum);
    }

    public void address(){
        System.out.println(address);
    }

    public static void main(String[] args) {
        child_1_Inheritance obj1 = new child_1_Inheritance();
        obj1.sum();
        obj1.bmi();
        obj1.myName();
    }
}
