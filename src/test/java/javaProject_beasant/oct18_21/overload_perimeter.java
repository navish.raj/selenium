package javaProject_beasant.oct18_21;

public class overload_perimeter {
    public static void main(String[] args) {
        overload_perimeter obj1 = new overload_perimeter();
        obj1.perimeter(10);
        obj1.perimeter(5);
    }

    public void perimeter(double radius){
        System.out.println("Area of the circle is " +(2*3.14*radius));
    }

    public void perimeter(int side){
        System.out.println("Perimeter of the square is " +(4*side));
    }
}
