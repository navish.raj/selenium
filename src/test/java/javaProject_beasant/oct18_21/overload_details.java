package javaProject_beasant.oct18_21;

public class overload_details {

    public static void main(String[] args) {
        overload_details obj1 = new overload_details();
        obj1.display("Navish", 34);
        obj1.display("Navish", "IS7375");
    }

    public void display(String name, int age){
        System.out.println("my name is " +name);
        System.out.println("my age is " +age);
    }

    public void display(String name, String id){
        System.out.println("my name is " +name);
        System.out.println("my employee id is " +id);
    }


}
