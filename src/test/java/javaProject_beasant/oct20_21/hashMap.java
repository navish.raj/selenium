package javaProject_beasant.oct20_21;

import java.util.HashMap;

public class hashMap {
    public static void main(String[] args) {
        HashMap<Double, Integer> hashMapexample = new HashMap<>();
        hashMapexample.put(4.3, 4);
        hashMapexample.put(5.3, 5);
        hashMapexample.put(2.3, 2);
        hashMapexample.put(1.3, 1);

        System.out.println(hashMapexample);

        hashMapexample.put(9.5, 8);

        System.out.println(hashMapexample);

        hashMapexample.remove(5.3);
        System.out.println(hashMapexample);

        for(double l: hashMapexample.keySet()){
            System.out.println(l+ ":" +hashMapexample.get(l));
        }
    }
}
