package javaProject_beasant.oct20_21;

import java.util.TreeMap;

public class treeMap {
    public static void main(String[] args) {
        TreeMap<Integer,String> treemapExample = new TreeMap<>();
        treemapExample.put(1, "A");
        treemapExample.put(2, "K");
        treemapExample.put(3, "C");
        treemapExample.put(4, "F");

        //System.out.println(treemapExample);

        //treemapExample.remove(1);
        //System.out.println(treemapExample);

        //System.out.println(treemapExample.keySet());
        //System.out.println(treemapExample.get(3));

        for(int l: treemapExample.keySet()){
            System.out.println(l+ ":" + treemapExample.get(l));
        }

    }
}
