package javaProject_beasant.oct19_21;

import java.util.concurrent.ArrayBlockingQueue;

public class collectionQueue {
    public static void main(String[] args) {
        ArrayBlockingQueue<Character> examplequeue = new ArrayBlockingQueue<>(5);
        examplequeue.add('A');
        examplequeue.add('B');
        examplequeue.add('C');
        examplequeue.add('D');
        examplequeue.add('E');
        for(char dummy: examplequeue){
            System.out.println(dummy);
        }
        examplequeue.remove();
        for(char dummy1: examplequeue){
            System.out.println(dummy1);
        }
        examplequeue.add('A');
        for(char dummy2: examplequeue){
            System.out.println(dummy2);
        }

        System.out.println(examplequeue.peek());
            }
}
