package javaProject_beasant.oct19_21;

import java.util.TreeSet;

public class collectionTreeset {
    public static void main(String[] args) {
        TreeSet<Double> exampleTreeset = new TreeSet<>();
        exampleTreeset.add(1.1);
        exampleTreeset.add(3.1);
        exampleTreeset.add(2.1);
        exampleTreeset.add(4.1);
        exampleTreeset.add(5.1);

        for(double dummy:exampleTreeset){
            System.out.println(dummy);
        }

        System.out.println("linebreak");
        exampleTreeset.add(3.6);

        for(double dummy1:exampleTreeset){
            System.out.println(dummy1);
        }

        System.out.println("linebreak");
        exampleTreeset.remove(3.1);

        for(double dummy2:exampleTreeset){
            System.out.println(dummy2);
        }
    }
}
