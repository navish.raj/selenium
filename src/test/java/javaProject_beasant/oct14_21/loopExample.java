package javaProject_beasant.oct14_21;

public class loopExample {

    public static void main(String[] args) {
        loopExample obj1 = new loopExample();
        //obj1.eighteenTable();
        //obj1.evenNumbers();
        //obj1.oddNumbers();
        //obj1.primeNumbers();
        obj1.fourteenTable();
    }

    public void eighteenTable(){
        int value = 18;
        while (value<200){
            System.out.println(value);
            value = value + 18;
        }
    }
    public void evenNumbers(){
    int value1 =20;
    while(value1 > 0){
        System.out.println(value1);
        value1-=2;
    }
    }

    public void oddNumbers(){
        int value2 = 1;
        while(value2 <= 31){
            System.out.println(value2);
            value2 +=2;
        }
    }

    public void primeNumbers(){
        for(int value3 = 30; value3>0; value3--){
            System.out.println(value3);
        }
    }

    public void fourteenTable(){
        int value = 14;
        while (value<=126){
            System.out.println(value);
            value = value + 14;
        }
    }
}
