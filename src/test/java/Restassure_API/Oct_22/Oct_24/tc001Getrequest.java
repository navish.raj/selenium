package Restassure_API.Oct_22.Oct_24;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

public class tc001Getrequest {
    @Test
    void getweatherDetails(){

        //Specify base URI
        RestAssured.baseURI ="http://restapi.demoqa.com/utilities/weather/city";

        //Request object
        RequestSpecification httpRequest = RestAssured.given();

        //Response object
        Response response = httpRequest.request(Method.GET, "/Hyderabad");

        //Print Response in console window
        String responseBody = response.getBody().asString();
        System.out.println("Response body is " +responseBody);

        //Validating statuscode and status line
        int statusCode = response.statusCode();
        Assert.assertEquals(statusCode, 200);
        String statusLine = response.statusLine();
        Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
    }

}
