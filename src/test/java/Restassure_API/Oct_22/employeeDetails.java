package Restassure_API.Oct_22;

import org.json.simple.JSONObject;
import org.openqa.selenium.json.Json;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import static org.hamcrest.Matchers.*;

public class employeeDetails {
        @Test
        public void test() {

            String baseUrl = "https://reqres.in/api/users?page=2";
            Response response = RestAssured.get(baseUrl);
            System.out.println(response.statusCode());
            int statusCode = response.getStatusCode();
            Assert.assertEquals(statusCode, 200);
        }

        @Test
        public void test1() {

            given()
                    //.contentType("application/json")
                    .when()
                    .get("https://reqres.in/api/users?page=2")
                    .then()
                    .statusCode(200)
                    .body("data.id[1]", equalTo(8));
        }


        @Test
        public void test3() {

            JSONObject request = new JSONObject();
            request.put("name", "gopi");
            request.put("job", "test");
            System.out.println(request);
            System.out.println(request.toString());
            given().
                    body(request.toJSONString()).
                    when().
                    post("https://reqres.in/api/users").
                    then()
                    .statusCode(201 );
        }
        @Test
        public void test2() {

            given().get("https://reqres.in/api/users?page=2").then().
                    statusCode(200).
                    body("data.id[1]", equalTo(8)).
                    body("data.first_name", hasItems("Michael", "Lindsay")).
                    log().all();
        }

    }

