package seleniumProject_beasant;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class oct26_21 {

    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/windows/chromedriver_94.exe");
        ChromeDriver driver = new ChromeDriver();

        //to get an url
        driver.get("https://www.google.com/");
        driver.navigate().to("https://www.google.com/");

        //how to minimize
        driver.manage().window().maximize();
        driver.manage().window().fullscreen();

        //to find the dimension
        Dimension size = driver.manage().window().getSize();
        System.out.println("height " +size.getHeight());
        System.out.println("width " +size.getWidth());

        //to find the position
        Point point = driver.manage().window().getPosition();
        System.out.println("X value" +point.getX());
        System.out.println("Y value" +point.getY());

        //to resize
        Dimension d = new Dimension(300,400);
        driver.manage().window().setSize(d);

        //types of closing
        driver.close();
        driver.quit();

        // to pause
        driver.wait(3000);
        Thread.sleep(3000);
        TimeUnit.SECONDS.sleep(3);

        // browser information
        Capabilities cap = driver.getCapabilities();
        cap.getBrowserName();
        cap.getVersion();


    }
}
