package seleniumProject_gopi.oct15_21;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class login {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/windows/chromedriver_94.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        String url = "http://demo.guru99.com/test/newtours/";
        driver.get(url);
        //Enter username
        driver.findElement(By.name("userName")).sendKeys("admin");
        //Enter Password
        driver.findElement(By.name("password")).sendKeys("admin");
        //Click submit button
        driver.findElement(By.name("submit")).click();
        //Verify successful login title
        String actualLoginTitle = driver.findElement(By.tagName("h3")).getText();
        String expectedLoginTitle = "Login Successfully";
        //Assert login title
        Assert.assertEquals(actualLoginTitle, expectedLoginTitle);
        driver.close();
    }
}
