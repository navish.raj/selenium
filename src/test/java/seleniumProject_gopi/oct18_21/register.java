package seleniumProject_gopi.oct18_21;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class register {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/windows/chromedriver_94.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        String url = "http://demo.guru99.com/test/newtours/#";
        driver.get(url);

        //Select on Register option
        //driver.findElement(By.linkText("REGISTER")).click();
        driver.findElement(By.xpath("//a[@href='register.php']")).click();
        //Verify register url opens successfully. Need id for verification for title
        //Enter contact information
        driver.findElement(By.name("firstName")).sendKeys("Navish");
        driver.findElement(By.name("lastName")).sendKeys("Raj");
        driver.findElement(By.name("phone")).sendKeys("9047691388");
        //Enter mailing information
        driver.findElement(By.id("userName")).sendKeys("admin1@guru99.com");
        driver.findElement(By.name("address1")).sendKeys("Plot No 16: Karpaga");
        driver.findElement(By.name("city")).sendKeys("Tirunelveli");
        driver.findElement(By.name("state")).sendKeys("Tamilnadu");
        driver.findElement(By.name("postalCode")).sendKeys("627007");
        Select dropdownCountry = new Select(driver.findElement(By.name("country")));

        dropdownCountry.selectByVisibleText("INDIA");
       // dropdownCountry.selectByValue("INDIA");
        //driver.findElement(By.name("country")).;
        //Enter user information
        driver.findElement(By.name("email")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin1");
        driver.findElement(By.name("confirmPassword")).sendKeys("admin1");
        driver.findElement(By.name("submit")).click();
        String actualText = driver.findElements(By.tagName("<b>")).get(3).getText();
        String expectedText = "Note: Your user name is admin1.";
        Assert.assertEquals(actualText,expectedText);










    }
}
