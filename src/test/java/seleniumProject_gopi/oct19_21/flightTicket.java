package seleniumProject_gopi.oct19_21;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class flightTicket {
    public static void main(String[] args) {
        //Initialize the chrome driver to establish connection
        System.setProperty("webdriver.chrome.driver", "src/main/resources/windows/chromedriver_94.exe");
        //Instantiate the Webdriver class
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        String url = "http://demo.guru99.com/test/newtours/#";
        //Enter url
        driver.get(url);
        //Select Flights
        driver.findElement(By.xpath("//a[@href='reservation.php']")).click();
        //Select flight type
        WebElement radio = driver.findElement(By.xpath("//input[@name='tripType' and @value='oneway']"));
        radio.click();
        //Select No of Passengers
        Select noPassengers = new Select(driver.findElement(By.name("passCount")));
        noPassengers.selectByValue("2");
        //Select Departure point
        Select departingFrom = new Select(driver.findElement(By.name("fromPort")));
        departingFrom.selectByValue("London");
        //Select travel month and day
        Select travelMonth = new Select(driver.findElement(By.name("fromMonth")));
        travelMonth.selectByValue("3");
        Select travelDate = new Select(driver.findElement(By.name("fromDay")));
        travelDate.selectByValue("14");
        //Select destination point
        Select reachingTo = new Select(driver.findElement(By.name("toPort")));
        reachingTo.selectByValue("Paris");
        //select travel class
        WebElement radio1 = driver.findElement(By.xpath("//input[@value='Business']"));
        radio1.click();
        //select airline
        Select airline = new Select(driver.findElement(By.name("airline")));
        airline.selectByVisibleText("Blue Skies Airlines");
        //search flights
        driver.findElement(By.name("findFlights")).click();
        System.out.println("success");

    }
}
