package seleniumProject_gopi.oct14_21;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class verifyTitle {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/windows/chromedriver_94.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        String url = "http://demo.guru99.com/test/newtours/";
        String expectedDemoSiteName = "Demo Site";
        driver.get(url);
        //verify guru site name
        String  actualDemoSiteName =  driver.findElement(By.id("site-name")).getText();
        Assert.assertEquals(actualDemoSiteName, expectedDemoSiteName);
        driver.close();
    }
}
